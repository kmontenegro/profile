## Ken Montenegro | kmontenegro@comeuppance.net  
### Summary  
Ken Montenegro is a technologist with a law degree. Ken's work centers around holistic and practical approaches to organizational efficiency, resiliency, safety, and security. He has also exercised various leadership positions in organizations created for social benefit and betterment.  

#### Education
2010 Juris Doctorate from Peoples College of Law  

#### Leadership Roles  
##### Current  
* Board Member, MayFirst Movement Technology   
* Advisor, Social Movements Technology 
##### Prior  
* Vice President, National Lawyers Guild 
* Board Member, NTEN  
* Board Member, Immigrant Defenders Law Center 
* Vice-Chair of Trustees, First Unitarian Church of Los Angeles  
* Advisor, Rampart JUICE  
* Vice President, National Lawyers Guild (Los Angeles Chapter)  
* Co-founder, Stop LAPD Spying Coalition  
#### Publications  
* 3 Translated poems published in: Skidrow Penthouse, and World Literature Today  
* 1 Debate published in McSweeney's Quarterly  
#### Presentations and Speaking Engagements (partial listing)  
* 2021 Hackers on Planet Earth (HOPE)
* 2020 One Justice California
* 2019 CLINIC Convening
* 2019 Internet Freedom Festival  
* 2019 Nonprofit Technology Conference  
* 2018 Colorado University School of Law
* 2018 Internet Freedom Festival  
* 2018 Nonprofit Technology Conference  
* 2017 Georgetown Law Center's Color of Surveillance  
* 2016 CUNY Law School 
#### Teaching
* 2019 Peoples College of Law  
### Employment  
* 2003-Current: Information Technology Director, Advancing Justice Los Angeles
* 2019-2020: Digital Security Coordinator, RoadMap Consulting  
* 2001-2003: Information Technology Director, Homeless Healthcare Los Angeles  
* 1999-2001: Special Projects, Maternal and Child Health Access
* 1999-2003: Technologist, Coalition for Humane Immigrant Rights of Los Angeles 
#### Freelance Work  
* 2017-2020: Independent Consultant  
* 2018-Current: RoadMap Affiliated Consultant (focused on worker centers & OrgSec)  
